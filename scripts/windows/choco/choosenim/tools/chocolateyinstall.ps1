$ErrorActionPreference = 'Stop'

$toolsDir = Split-Path $MyInvocation.MyCommand.Definition

$silentArgs = @('/sp- /silent /norestart')

$packageArgs = @{
  packageName    = 'choosenim'
  fileType       = 'exe'
  file64         = "$toolsDir\choosenim-@VERSION@-amd64-setup.exe"
  checksum       = '@CHECKSUM@'
  checksumType   = 'sha256'
  silentArgs     = $silentArgs
  validExitCodes = @(0)
}

Install-ChocolateyInstallPackage @packageArgs
