# Change location
$currentLocation = Get-Location

# Get latest release
$latestReleaseJson = (Invoke-RestMethod -Method GET -Uri https://api.github.com/repos/nim-lang/choosenim/releases/latest).assets
$downloadURI = ($latestReleaseJson | Where-Object name -like *_windows_amd64.exe).browser_download_url
$downloadURI -match '(\d+)\.(\d+)\.(\d+)'
$tagName = $Matches[0]
Write-Host Exe version to download -> $downloadURI

# Download
Set-Location .\scripts\windows\inno
Invoke-WebRequest -Uri $downloadURI -UseBasicParsing -Out choosenim.exe
Invoke-WebRequest -Uri https://raw.githubusercontent.com/nim-lang/choosenim/master/LICENSE -Out LICENSE

# Run Inno Setup
ISCC.exe inno-choosenim-setup.iss /DVERSION_NAME=$tagName

# Rename
Rename-Item -Path ".\bin\choosenim-setup.exe" -NewName "choosenim-$tagName-amd64-setup.exe"

# Clean
Remove-Item choosenim.exe
Remove-Item LICENSE

# Change location
Set-Location $currentLocation

# Write version in env file
Add-Content -Path version.env -Value "CHOOSENIM_VERSION=$tagName"

# Move exe
New-Item -Path ".\upload-dir" -ItemType Directory
Move-Item -Path ".\scripts\windows\inno\bin\choosenim-$tagName-amd64-setup.exe" -Destination ".\upload-dir"