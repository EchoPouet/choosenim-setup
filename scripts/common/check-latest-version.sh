#!/bin/bash

exeLatestRelease=$(curl -s https://api.github.com/repos/nim-lang/choosenim/releases/latest | grep 'amd64.exe' | grep 'browser_' | cut -d\" -f4)
exeToUpload=$(basename -- $exeLatestRelease)
exeVersion=$(echo $exeToUpload | sed 's/[^0-9.]*\([0-9.]*\).*/\1/')

# Check only if inno setup version exists
function check_file_exists()
{
    result=$(wget -S --spider --header "JOB-TOKEN: $CI_JOB_TOKEN" ${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/packages/generic/choosenim/$exeVersion/choosenim-$exeVersion-amd64-setup.exe  2>&1 | grep "Remote file exists")
    if [ "$result" != "" ]; then
        return 0
    else
        return 1
    fi
}

if check_file_exists ; then
    echo "Windows setup already exists with this version"
else
    echo "Version missing, run trigger"
    curl --request POST --form token=${JOB_TRIGGER_TOKEN} --form ref=${CI_COMMIT_REF_NAME} --form "variables[GENERATE_SETUP]=true" "${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/trigger/pipeline"
fi

# Write version in file
echo "CHOOSENIM_VERSION=$exeVersion" > version.env